#!/bin/bash

grep -q 80.80.80.80 /etc/resolv.conf

if [ $? -eq 0 ]
then
  echo "Success: 80.80.80.80 was found in resolv.conf"
else
  echo "Failure: 80.80.80.80 was not found in resolv.conf"
fi

print_to_stdout() {
  echo $1
}

print_to_stderr() {
  echo $1 >&2
}

read -p 'Enter text:' msg
print_to_stdout $msg
print_to_stderr $msg

